package jc.example.com.tp_startactivitywithparameters;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

// Activité appelée
public class MonActivite extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mon_activite);

        // on regarde si des paramètres ont été passés lors de l'appel
        Bundle extra = this.getIntent().getExtras();
        if(extra != null)
        {
            // si oui, alors on récupère le paramètre voulu
            String data = extra.getString("maclé");
            ((TextView)findViewById(R.id.monTexte01)).setText(data);
        }
    }
}
