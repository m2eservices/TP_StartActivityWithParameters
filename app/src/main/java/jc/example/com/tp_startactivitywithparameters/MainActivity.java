package jc.example.com.tp_startactivitywithparameters;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(this,MonActivite.class);
        intent.putExtra("maclé", "Oui ça marche !");
        startActivity(intent);
    }
}
